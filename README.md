# De Pitloze Duif documentatie

Nog niet alle onderdelen zijn volledig geimplementeerd. Zo kunnen er nog geen records worden geupdate. Is er nog geen API Authenticatie (op de Access-Control-Allow-Origin header na, die nagaat of de API call van de ingestelde Frontend komt). En mis ik hier en daar nog error handling en validatie.

#### Design Patterns
De database connection class src/Database/Database.php is de verplichte Singleton.
De Iterator is te vinden in src/Iterators/PigeonIterator.php, en wordt gebruikt om de data van de duiven uit te breiden met extra informatie.

#### Database ontwerp
Het Database ontwerp is te vinden in: resources/database/de_pitloze_duif.sql

#### API Documentatie

De API documentatie is de vinden in API-DOCS.md

####

Inloggen: admin@example.com / admin123

## Installatie:

Kopieer config.example.yaml naar config.yaml
En vul hier o.a. de juiste urls in voor de API, Frontend en de Backend.
De Frontend kan alleen met de API communiceren als deze juist zijn ingevult.

Kopieer database.example.yaml naar database.yaml
Vul hier de juiste database credentials in.

Run vervolgens:
`composer install`

Er worden 2 packeges geinstalleerd:
* twig/twig
* symfony/yaml

***Twig:*** https://twig.symfony.com/ 
Dit is een Templating engine die ik erg prettig vind werken. Zeer flexibel en eenvoudig te implementeren in je applicatie

***Yaml:*** https://symfony.com/doc/current/components/yaml.html
Deze gebruik ik voor het uitlezen van mijn config bestanden. Dit zijn allemaal .yaml files en kunnen met deze package eenvoudig worden omgezet naar arrays. 

Ik heb de config files in yaml format gezet omdat ik dit een goed leesbaar format vind.
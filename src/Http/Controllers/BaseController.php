<?php

namespace BVW\Http\Controllers;

use PDO;
use Twig\Environment;

class BaseController
{
    protected $config;
    protected $db;
    protected $twig;
    protected $modelName;
    protected $twigBasePath;
    
    /**
    * List of restricted methods that need authentication
    * if all methods are resticted, simply set the value to ['*']
    */
    protected $restrictedMethods = [];
    
    /**
    * Initialize controller
    * 
    * @param array $config
    * @param Connection $db
    * @param Environment $twig
    * @param string $environment
    */
    public function __construct(array $config, PDO $db, Environment $twig, string $environment)
    {
        $this->config = $config;
        $this->db = $db;
        $this->twig = $twig;
        $this->twigBasePath = $environment;
        
        if (!empty($this->modelName)) {
            $className = "\\BVW\\Models\\".$this->modelName;
            $this->{$this->modelName} = new $className($db);    
        }
        
    }
    
    /**
    * @param mixed $name
    * @param mixed $arguments
    */
    public function __call($name, $arguments)
    {
        $this->notFound();
    }
    
    /**
    * Page not found
    * Throw 404 error
    * 
    * @todo render view
    */
    public function notFound()
    {
        header('HTTP/1.0 404 Not Found');
        return "404 Not Found";
    }
    
    /**
    * HTTP Method not allowed
    * Throw 405 error
    * 
    * @todo render view
    */
    public function notAllowed()
    {
        header('HTTP/1.0 405 Not Allowed');
        return "405 Not Allowed";
    }
    
    /**
    * HTTP forbidden
    * Throw 403 error
    * 
    * @todo render view
    */
    public function forbidden()
    {
        header('HTTP/1.0 403 Forbidden');
        return "403 You don't have permission to access this location";
    }
    
    /**
    * Render twig template
    * 
    * @param mixed $path
    * @param mixed $parced_variables
    */
    protected function twigRender(string $path, array $parced_variables = []) 
    {
        // Set main variables accessible in all application views
        $main_variables = [
            'app' => [
                'application_scheme' => $this->config['application_scheme'],
                'api_url' => $this->config['api_url'],
                'csrf_token' => $_SESSION['csrf_token'],
            ]
        ];
        
        // Merge parced variables with the main variables (Parced have more priority so should override the main variables) 
        $variables = $parced_variables + $main_variables;
        
        // Render Twig template
        return $this->twig->render($path, $variables);
    }
    
    /**
    * Return json encoded array with headers
    * 
    * @param mixed $array
    */
    protected function jsonResponse(array $array)
    {
        /**
        * Quick and dirty! Must be fixed
        * 
        * @todo: make response class
        */
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        header('Access-Control-Allow-Origin: '.$this->config['application_scheme'].$this->config['front_url']);
        header('Content-Type: application/json');
        return json_encode($array, true);
    }
    
    /**
    * Returns the Request Payload in array format
    * 
    * @return array
    */
    public function getRequestPayload()
    {
        $request_payload = file_get_contents('php://input');
        return (array) json_decode($request_payload);
    }
    
    /**
    * Check if method is restricted
    * Restricted methods are defined in the protected restrictedMethods variable
    * 
    * @param mixed $method
    * 
    * @return bool
    */
    public function isMethodRestricted(string $method): bool
    {
        if (!empty($method)) {
            if (in_array('*', $this->restrictedMethods) || in_array($method, $this->restrictedMethods)) {
                return true;
            }
        }
        
        return false;
    }
    
}
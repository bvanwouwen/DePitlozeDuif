<?php

namespace BVW\Http\Controllers\Api;

use BVW\Http\Controllers\PigeonsBaseController;
use BVW\Iterators\PigeonIterator;

class PigeonsController extends PigeonsBaseController
{
    
//    protected $restrictedMethods = [
//        '*'
//    ];
    
    /**
    * Pigeons overview
    * 
    * @param List pigeons from specific owner id
    */
    public function index(int $ownerId = null)
    {
        if (is_numeric($ownerId)) {
            $pigeons_result = $this->Pigeon->getByField('owner_id', $ownerId);
        } else {
            $pigeons_result = $this->Pigeon->getAll();
        }
        
        $pigeons = new PigeonIterator($pigeons_result, $this->Pigeon);
        foreach ($pigeons as $key => $pigeon) {
            $result['pigeons'][$key] = $pigeon;
        }

        return $this->jsonResponse($result);
    }
    
    /**
    * Save new pigeon
    * 
    * @return new pigeon or error
    */
    public function store()
    {
        $data = $this->getRequestPayload();
        
        if (!empty($data)) {
            /**
            * Validate posted data
            */
            $errors = $this->Pigeon->validate($data);
            if (!empty($errors)) {
                // return array with error messages 
                return $this->jsonResponse($errors);
            }
            
            $new_pigeon = $this->Pigeon->insert([
                'owner_id' => $data['owner_id'],
                'name' => $data['name'],
                'date_of_birth' => $data['date_of_birth'],
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ]);
            
            if ($new_pigeon) {
                // Yugh, we need a factory for this, but HEY look at the time!
                $pigeons = new PigeonIterator([$new_pigeon], $this->Pigeon);
                foreach ($pigeons as $key => $pigeon) {
                    $result = $pigeon;
                }
                            
                return $this->jsonResponse($result);
            }
            
            return $this->jsonResponse(['errors' => ['Er ging iets mis met het opslaan van de duif.']]);
        
        }
        
        return $this->jsonResponse(['errors' => ['Vul A.U.B. het formulier in.']]);
    }
    
    /**
    * Get pigeon by id
    * 
    * @param mixed $pigeonId
    */
    public function view(int $pigeonId)
    {
        if (is_numeric($pigeonId)) {
            
            $pigeon = $this->Pigeon->getByField('id', $pigeonId, true);
            
            if ($pigeon) {
                $pigeons = new PigeonIterator([$pigeon], $this->Pigeon);
                foreach ($pigeons as $key => $pigeon) {
                    $result = $pigeon;
                }
                
                return $this->jsonResponse($result);
            }
            
        }
        
        return $this->jsonResponse(['errors' => 'Duif met intern nummer #'.$pigeonId.' niet gevonden.']);
    }
    
    /**
    * Delete pigeon by id
    * 
    * @todo: error handling
    */
    public function delete()
    {
        $data = $this->getRequestPayload();

        if (!empty($data) && isset($data['id'])) {
            $deleted = $this->Pigeon->delete($data['id']);
        }

        return $this->jsonResponse([]);
    }

}

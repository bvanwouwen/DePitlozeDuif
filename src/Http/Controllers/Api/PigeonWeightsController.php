<?php

namespace BVW\Http\Controllers\Api;

use BVW\Http\Controllers\PigeonWeightsBaseController;

class PigeonWeightsController extends PigeonWeightsBaseController
{

//    protected $restrictedMethods = [
//        '*'
//    ];

    /**
    * Pigeon wight history overview
    * 
    * @param List wight history from specific pigeon id
    */
    public function index(int $pigeonId)
    {
        $result = [];
        if (is_numeric($pigeonId)) {
            $weightHistories = $this->PigeonWeight->getByField('pigeon_id', $pigeonId);
        }
        
        return $this->jsonResponse($weightHistories);
    }
    
    /**
    * Save new pigeon
    * 
    * @param int $pigeonId
    * 
    * @return new pigeon or error
    */
    public function store(int $pigeonId)
    {
        header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
        header('Access-Control-Allow-Origin: '.$this->config['application_scheme'].$this->config['front_url']);
        
        $data = $this->getRequestPayload();
        
        if (!empty($data)) {
        
            /**
            * Validate posted data
            */
            $errors = $this->PigeonWeight->validate($data);
            if (!empty($errors)) {
                // return array with error messages 
                return $this->jsonResponse($errors);
            }
            
            $weight = $this->PigeonWeight->insert([
                'pigeon_id' => $pigeonId,
                'weight' => $data['weight'],
                'weight_date' => $data['weight_date'],
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ]);
            
            if ($weight) {
                return $this->jsonResponse($weight);
            }
            
            return $this->jsonResponse(['errors' => ['Er ging iets mis met het opslaan van de gewichtmeting.']]);
        }
        
        return $this->jsonResponse(['errors' => ['Vul A.U.B. het formulier in.']]);
    }
    
    /**
    * Delete weight registration by id
    * 
    * @todo: error handling
    */
    public function delete()
    {
        $data = $this->getRequestPayload();

        if (!empty($data) && isset($data['id'])) {
            $deleted = $this->PigeonWeight->delete($data['id']);
        }

        return $this->jsonResponse([]);
    }
    
}

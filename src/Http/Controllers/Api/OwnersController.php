<?php

namespace BVW\Http\Controllers\Api;

use BVW\Http\Controllers\OwnersBaseController;

class OwnersController extends OwnersBaseController
{    
    
//    protected $restrictedMethods = [
//        '*'
//    ];
    
    /**
    * Get all owners
    */
    public function index()
    {
        $owners = $this->Owner->getAll();
        return $this->jsonResponse($owners);
    }
    
    public function store()
    {
        $data = $this->getRequestPayload();

        if (!empty($data)) {
            
            /**
            * Validate posted data
            */
            $errors = $this->Owner->validate($data);
            if (!empty($errors)) {
                // return array with error messages 
                return $this->jsonResponse($errors);
            }
            
            $owner = $this->Owner->insert([
                'name' => $data['name'],
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ]);
            
            if ($owner) {
                return $this->jsonResponse($owner);
            }
            
            return $this->jsonResponse(['errors' => ['Er ging iets mis met het opslaan van de Eigenaar.']]);
        
        }
        
        return $this->jsonResponse(['errors' => ['Vul A.U.B. het formulier in.']]);
    }
    
    /**
    * Seach owners by name
    */
    public function getListByName()
    {        
        $owners = $this->Owner->getOwnerByName($_GET['search']);
        
        $data = [];
        foreach ($owners as $owner) {
            $data[] = [
                'id' => $owner['id'],
                'text' => $owner['name'],
            ];
        }
        
        return $this->jsonResponse($data);
    }
    
    /**
    * Delete owner by id
    * 
    * @todo: error handling
    */
    public function delete()
    {
        $data = $this->getRequestPayload();

        if (!empty($data) && isset($data['id'])) {
            $deleted = $this->Owner->delete($data['id']);
        }

        return $this->jsonResponse([]);
    }
    
}

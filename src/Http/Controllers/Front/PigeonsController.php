<?php

namespace BVW\Http\Controllers\Front;

use BVW\Http\Controllers\PigeonsBaseController;

class PigeonsController extends PigeonsBaseController
{
    protected $restrictedMethods = ['*'];
    
    /**
    * Pigeons overview
    * If owner id is given, list pigeons from specific owner
    * 
    * @param $ownerId
    */
    public function index($ownerId = null)
    {
        return $this->twigRender($this->twigBasePath.'/Pigeons/index.twig', [
            'ownerId' => $ownerId
        ]);
    }  
      
    /**
    * Pigeon view
    * 
    * @param $pigeonId
    */
    public function view($pigeonId = null)
    {
        return $this->twigRender($this->twigBasePath.'/Pigeons/view.twig', [
            'pigeonId' => $pigeonId
        ]);
    }
}

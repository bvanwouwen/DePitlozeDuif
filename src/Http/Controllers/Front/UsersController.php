<?php

namespace BVW\Http\Controllers\Front;

use BVW\Auth\Auth;
use BVW\Http\Controllers\UsersBaseController;

class UsersController extends UsersBaseController
{
    
    /**
    * If user is not logged in, show login form
    * else redirect to Owners/index
    */
    public function login()
    {
        $auth = new Auth($this->db);
        
        if ($auth->isAuthenticated()) {
            header("location: /owners/index");
            exit;
        }
        
        return $this->twigRender($this->twigBasePath.'/Users/login.twig');
    }
    
    /**
    * PW: admin123
    * 
    */
    public function authenticate()
    {
        /**
        * @todo: Validation on fields and csrf_token
        */
        
        $auth = new Auth($this->db);
        if ($auth->authenticate($_POST['email_address'], $_POST['password'])) {
            header("location: /owners/index");
            exit;
        }
    }
}

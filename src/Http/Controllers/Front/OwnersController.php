<?php

namespace BVW\Http\Controllers\Front;

use BVW\Http\Controllers\OwnersBaseController;

class OwnersController extends OwnersBaseController
{
    protected $restrictedMethods = [
        'index'
    ];
    
    /**
    * Owners overview
    */
    public function index()
    {
        return $this->twigRender($this->twigBasePath.'/Owners/index.twig');
    }
}

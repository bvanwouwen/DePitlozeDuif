<?php

namespace BVW\Providers;

use BVW\Application;
use BVW\Database\Database;
use Symfony\Component\Yaml\Yaml;
use Twig_Loader_Filesystem;
use Twig_Environment;
use RuntimeException;

class ServiceProvider
{
    public function __construct(Application $app)
    {
        // Set application config
        $app->set('config', (function() {
            
            $file = dirname(dirname(dirname(__FILE__))).'/config/config.yaml';
            
            if (!file_exists($file)) {
                throw new RuntimeException('Config file not found');
            }
            
            return Yaml::parseFile($file);
            
        }));
        
        // Setup database connection
        $app->set('db', (function() {
            
            $file = dirname(dirname(dirname(__FILE__))).'/config/database.yaml';
            
            if (!file_exists($file)) {
                throw new RuntimeException('Database file not found');
            }
               
            $dbCredentials = Yaml::parseFile($file);
            $db = Database::getInstance($dbCredentials);
            return $db;
            
        }));
        
        //Set defined Routes
        $app->set('routes', (function() {
            
            $file = dirname(dirname(dirname(__FILE__))).'/config/routes.yaml';
            
            if (!file_exists($file)) {
                throw new RuntimeException('Route file not found');
            }
            
            return Yaml::parseFile($file);
            
        }));
        
        // Instantiate Twig template environment
        $app->set('twig', (function() {
            
            $dir = dirname(dirname(dirname(__FILE__))).'/resources/templates';
            
            if (!is_dir($dir)) {
                throw new RuntimeException('Template directory not found');
            }
            
            // Specify Twig templates location
            $loader = new Twig_Loader_Filesystem($dir);
            
            return new Twig_Environment($loader);
        }));
        
        // Determine if we need to show errors
        if ($app->get('config')['environment'] == 'development') {
            ini_set('display_error', 1);
            error_reporting(E_ALL^E_NOTICE);
        } else {
            ini_set('display_error', 0);
            error_reporting(0);
        }
    }
}
<?php

namespace BVW\Models;

use BVW\Models\Pigeon;

class Owner extends Model
{
    protected $tableName = 'owners';
    
    /**
    * Validate owner data
    * 
    * @param array $data
    * 
    * @retrun array
    */
    public function validate(array $data): array
    {
        $errors = [];

        // Validate name, can not be empty
        if (!isset($data['name']) || empty($data['name'])) {
            $errors['errors'][] = 'Naam is verplicht';
        }
        
        // Validate if user exists
        if (isset($data['id']) && is_numeric($data['id'])) {
            $owner = $this->getByField('id', $id, true);
            if (isset($data['id']) && is_numeric($data['id']) && !empty($owner)) {
                $errors['errors'][] = 'Eigenaar bestaat niet';
            }
        }

        return $errors;
        
    }
    
    /**
    * Get owners by name filtering on name
    * 
    * @param mixed $name
    * 
    * @return array
    */
    public function getOwnerByName($name): array
    {
        $stmt = $this->db->prepare('SELECT * FROM '.$this->tableName.' WHERE name LIKE :name');
        $stmt->bindValue(':name', "%".$name."%", $this->db::PARAM_STR);
        $stmt->execute();
        return $stmt->fetchAll() ?: [];
    }
    
    public function delete($ownerId)
    {
        if (is_numeric($ownerId)) {
                        
            // First we remove the owners pigeons
            $pigeonModel = new Pigeon($this->db);
            $pigeonsDeleted = $pigeonModel->deletePigeonsByOwnerId($ownerId);
            
            // Delete Pigeon
            $this->deleteEntity($ownerId);
            
        }
    }
    
}

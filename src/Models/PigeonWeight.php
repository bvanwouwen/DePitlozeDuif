<?php

namespace BVW\Models;

use BVW\Services\Validation;

class PigeonWeight extends Model
{
    protected $tableName = 'pigeon_weights';
    
    /**
    * Validate pigeon data
    * 
    * @param array $data
    * 
    * @retrun array
    */
    public function validate(array $data): array
    {
        $errors = [];

        // Validate weight, can not be empty, must be numeric
        if (!isset($data['weight']) || !is_numeric($data['weight'])) {
            $errors['errors'][] = 'Gewicht is verplicht, vul een numerieke waarde in.';
        }

        // Validate date of weighing, must be valid date format
        if (!Validation::validateDate($data['weight_date']??'')) {
            $errors['errors'][] = 'Weegdatum is in een incorrect format';
        }

        return $errors;
        
    }
    
    /**
    * Delete pigeon weight by pigeon id
    * Check for existing pigeon weights and remove them in a loop
    * 
    * @param mixed $pigeonId
    */
    public function deletePigeonWeightsByPigeonId($pigeonId)
    {
        if (is_numeric($pigeonId)) {
            
            // Check if pigeon has registered weights
            $weights = $this->getByField('pigeon_id', $pigeonId);
            foreach ($weights as $weight) {
                // Remove all weight registries
                $this->delete($weight['id']);
            }
        }
    }
    
    /**
    * Delete pigeon weight registry
    * 
    * @param mixed $weightId
    */
    public function delete($weightId)
    {
        if (is_numeric($weightId)) {
            // Delete Pigeon weight
            $this->deleteEntity($weightId);
        }
    }
}

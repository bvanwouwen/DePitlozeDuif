<?php

namespace BVW\Models;

use PDO;

abstract class Model {
    
    protected $db;
    protected $tableName;
    
    public function __construct(PDO $db)
    {
        $this->db = $db;
    }
    
    /**
    * Get all records from model
    * 
    * @return array
    */
    public function getAll(): array
    {
        $stmt = $this->db->prepare('SELECT * FROM '.$this->tableName);
        $stmt->execute();
        return $stmt->fetchAll() ?: [];
    }
    
    /**
    * Get record from the db with dynamic field matching the given value
    * 
    * @param mixed $field
    * @param mixed $value
    * @param mixed $single
    * 
    * @return mixed
    */
    public function getByField(string $field, string $value, $single = false): array
    {
        if (!empty($field)) {
        
            $stmt = $this->db->prepare('SELECT * FROM '.$this->tableName.' WHERE '.$field.' = :value');
            $stmt->execute(['value' => $value]);
            
            if (!$single) {
                return $stmt->fetchAll() ?: [];    
            }
            
            return $stmt->fetch() ?: [];
        
        }
        
        return [];
    }
    
    /**
    * Update given values for given id
    * 
    * @param mixed $id
    * @param mixed $values
    */
    public function update(int $id, array $values)
    {
        if (!empty($id) && !empty($values)) {
            
            $sql = "UPDATE ".$this->tableName." SET ";
            
            // Set updated fields
            $fields = '';
            foreach ($values as $key => $val) {
                $fields .= $key." = :".$key.",";
            }
            
            // Remove trailing comma
            $sql .= trim($fields, ',');
            $sql .= " WHERE id = :id";
            
            // Add $id to the values array
            $values = ['id' => $id] + $values;
            
            $stmt = $this->db->prepare($sql);
            $stmt->execute($values);  
            
        }
        
        return false;
    }
    
    /**
    * Insert given values
    * 
    * @param mixed $values
    */
    public function insert(array $values)
    {
        if (!empty($values)) {
            
            $sql = "INSERT INTO ".$this->tableName." SET ";
            
            // Set updated fields
            $fields = '';
            foreach ($values as $key => $val) {
                $fields .= $key." = :".$key.",";
            }
            
            // Remove trailing comma
            $sql .= trim($fields, ',');
            
            $stmt = $this->db->prepare($sql);
            $stmt->execute($values);  
            
            $id = $this->db->lastInsertId();
            
            return ['id' => $id] + $values;
        }
        
        return false;
    }
    
        
    /**
    * Remove entity by id
    * 
    * @param mixed $entityId
    */
    public function deleteEntity($entityId)
    {
        if (is_numeric($entityId)) {
            $stmt = $this->db->prepare('DELETE FROM '.$this->tableName.' WHERE id = :value');
            $stmt->execute(['value' => $entityId]);
        }
    }
    
}
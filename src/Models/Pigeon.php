<?php

namespace BVW\Models;

use BVW\Models\Owner;
use BVW\Models\PigeonWeight;
use BVW\Services\Validation;
use DateTime;

class Pigeon extends Model
{
    protected $tableName = 'pigeons';
    
    public function Owner($ownerId): array
    {
        if (is_numeric($ownerId)) {
            $stmt = $this->db->prepare('SELECT * FROM owners WHERE id = :ownerId');
            $stmt->execute(['ownerId' => $ownerId]);
            return $stmt->fetch() ?: [];
        }
        
        return [];
    }
    
    /**
    * Validate pigeon data
    * 
    * @param array $data
    * 
    * @retrun array
    */
    public function validate(array $data): array
    {
        $errors = [];

        // Validate owner, can not be empty
        if (!isset($data['owner_id']) || empty($data['owner_id'])) {
            $errors['errors'][] = 'Eigenaar is verplicht';
        }

        // Validate name, can not be empty
        if (!isset($data['name']) || empty($data['name'])) {
            $errors['errors'][] = 'Naam is verplicht';
        }

        // Validate date of birth, must be valid date format
        if (!isset($data['date_of_birth']) || empty($data['date_of_birth'])) {
            $errors['errors'][] = 'Geboortedatum is verplicht';
        }

        // Validate date of birth, must be valid date format
        if (!Validation::validateDate($data['date_of_birth']??'')) {
            $errors['errors'][] = 'Geboortedatum is een incorrect format';
        }
        
        // Validate if pigeon exists
        if (isset($data['id']) && is_numeric($data['id'])) {
            $pigeon = $this->getByField('id', $id, true);
            if (isset($data['id']) && is_numeric($data['id']) && !empty($pigeon)) {
                $errors['errors'][] = 'Pigeon bestaat niet';
            }
        }

        return $errors;
        
    }
    
    /**
    * Delete pigeon by owner id
    * Check for existing pigeons and remove them in a loop
    * 
    * @param mixed $ownerId
    */
    public function deletePigeonsByOwnerId($ownerId)
    {
        if (is_numeric($ownerId)) {
            
            // Check if owner has pigeons
            $pigeons = $this->getByField('owner_id', $ownerId);

            foreach ($pigeons as $pigeon) {
                // Delete Pigeon
                $this->delete($pigeon['id']);
            }
                        
        }
    }
    
    /**
    * Delete pigeon and linkes weight registries
    * 
    * @param mixed $pigeonId
    */
    public function delete($pigeonId)
    {
        if (is_numeric($pigeonId)) {
            
            // First we remove the pigeon weight registries
            $pigeonWeightModel = new PigeonWeight($this->db);
            $pigeonsDeleted = $pigeonWeightModel->deletePigeonWeightsByPigeonId($pigeonId);
            
            // Delete Pigeon
            $this->deleteEntity($pigeonId);
        }
    }
    
    /**
    * Get the pigeons owner
    * 
    * @param int $ownerId
    * 
    * @return array
    */
    public function getOwner(int $ownerId): array
    {
        $ownerModel = new Owner($this->db);
        return $ownerModel->getByField('id', $ownerId, true) ?: [];
    }
    
    /**
    * Get the pigeons age in months
    * 
    * @param string $date_of_birth
    * 
    * @return int total months
    */
    public function getAgeInMonths(string $date_of_birth): int
    {
        $total_months = 0;
        if (Validation::validateDate($date_of_birth)) {
            // Set date of birth object
            $birth = new \DateTime($date_of_birth);
            
            // Set current date
            $now = new \DateTime;
            
            // Get difference in dates 
            $difference = $now->diff($birth);
            
            // Calculate months
            $total_months = ($difference->y * 12) + $difference->m;
        }
        
        return $total_months;
    }
    
    /**
    * Get the pigeons weight history
    * 
    * @param id $pigeonId
    * 
    * @return array
    */
    public function getWeightHistory(int $pigeonId): array
    {
        return [];
    }
}

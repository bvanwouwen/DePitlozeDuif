<?php

namespace BVW\Iterators;

use Countable;
use Iterator;

class PigeonIterator implements Iterator
{
    private $pigeons;
    private $pigeonModel;
    private $index = 0; // number of current record
    private $item; // current item in the collection

    public function __construct($pigeons, $pigeonModel)
    {        
        $this->pigeons = $pigeons;
        $this->pigeonModel = $pigeonModel;
        $this->setPigeon();
    }

    private function setPigeon() 
    {
        /**
        * @todo: create a pigeon factory class
        */
        if (isset($this->pigeons[$this->index-1])) {
            $this->item = $this->pigeons[$this->index-1];
            $this->item['age_in_months'] = $this->pigeonModel->getAgeInMonths($this->item['date_of_birth']);
            $this->item['owner'] = $this->pigeonModel->getOwner($this->item['owner_id']);
            $this->item['weights'] = $this->pigeonModel->getWeightHistory($this->item['id']);

            return $this->item;
        }
    }
    
    public function count()
    {
        return count($this->pigeons);
    }
    
    public function rewind()
    {
        if ($this->index >= 0) $this->setPigeon();
        $this->next();
    }
    
    public function next()
    {
        $this->index++;
        $this->item = $this->setPigeon();
        
    }
    
    public function current()
    {
        return $this->item;
    }

    public function key()
    {
        return $this->index;
    }
    
    public function valid()
    {
        return ($this->index <= $this->count());
    }
}
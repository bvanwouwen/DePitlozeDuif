<?php

namespace BVW\Routing;

use BVW\Auth\Auth;
use BVW\Http\Controllers\BaseController;
use Twig\Environment;
use PDO;

class Routing
{   
 
    protected $config;
    protected $db;
    protected $twig;
    protected $routes;
    protected $environment;
    
    /**
    * Initialize routing class
    * 
    * @param array $config
    * @param Connection $db
    * @param Environment $twig
    * @param array $routes
    */
    public function __construct(array $config, PDO $db, Environment $twig, array $routes)
    {
        $this->config = $config;
        $this->db = $db;
        $this->twig = $twig;
        $this->routes = $routes;
    }
    
    /**
    * Run routing
    * 
    * @return content
    */
    public function run()
    {
        if ($namespace = $this->determineRoutingNamespace()) {
            
            $uri = $this->getUriPath();
            $uri_parts = $this->getUriPathParts($uri);

            return $this->matchRoute($namespace, $uri_parts);
            
        } 
            
        return $this->routeNotAllowed();
    }
    
    /**
    * Determine the namespace for the environment we are in
    */
    private function determineRoutingNamespace()
    {
        if ($_SERVER['SERVER_NAME'] == $this->config['front_url']) {
            $this->set('environment', 'front');
            return "BVW\\Http\\Controllers\\Front\\";
        } else if ($_SERVER['SERVER_NAME'] == $this->config['backend_url']) {
            $this->set('environment', 'admin');
            return "BVW\\Http\\Controllers\\Admin\\";
        } else if ($_SERVER['SERVER_NAME'] == $this->config['api_url']) {
            $this->set('environment', 'api');
            return "BVW\\Http\\Controllers\\Api\\";
        }
        
        return false;
    }
    
    /**
    * Get the uri path
    * 
    * @return string $uri 
    */
    protected function getUriPath(): string
    {
        $uri = $_SERVER['REQUEST_URI'] ?: '';
        return rawurldecode(parse_url($uri)['path']);        
    }
    
    /**
    * Get each single part of the uri
    * 
    * @param $uri 
    * @param array
    */
    protected function getUriPathParts(string $uri): array
    {
        /**
        * Get each part between the /
        */
        if (preg_match_all('#[^/]+#i', $uri, $matches)) {
            return $matches[0];
        }  
        
        return [];
    }
    
    /**
    * Get each single part of the uri
    * 
    * @param $namespace 
    * @param $parts 
    * 
    * @return view
    */
    public function matchRoute(string $namespace, array $parts)
    {
        /**
        * Loop over the routes to get a match with current page
        */
        foreach ($this->routes[$this->environment] as $route) {
            $uri_path = '/'.$parts[0].(isset($parts[1]) ? '/'.$parts[1] : '/index');

            // Check if path is the same as current route
            if (strtolower($uri_path) == strtolower($route['path'])) {

                // Check if controller class exists
                $class = $namespace.$route['controller'];
                if (class_exists($class)) {
                    $controller = new $class($this->config, $this->db, $this->twig, $this->environment);
                                        
                    // Check if method exists in controller
                    if (!method_exists($controller, $route['action'])) {
                        // 404 Not Found
                        return $this->routeNotFound();
                    }
                    
                    // Check if acces to method is restricted
                    if ($controller->isMethodRestricted($route['action'])) {
                        $auth = new Auth($this->db);
                        if (!$auth->isAuthenticated()) {
                            return $this->routeForbidden();
                        }
                    }
                    
                    // Unset class and method part, what remains are the parameters
                    unset($parts[0], $parts[1]);
                    
                    // 200 Call method
                    return call_user_func_array([$controller, $route['action']], array_values($parts));
                }
            }
        }

        // 404 Not Found
        return $this->routeNotFound();
        
    }
    
    /**
    * Call not found method on base controller
    * 
    * @return error page with 404 header Page Not Found
    */
    public function routeNotFound()
    {
        $controller = new BaseController($this->config, $this->db, $this->twig, $this->environment);
        return $controller->notFound();
    }
    
    /**
    * Call not allowed method on base controller
    * 
    * @return error page with 405 header Method Not Allowed
    */
    public function routeNotAllowed()
    {
        $controller = new BaseController($this->config, $this->db, $this->twig, $this->environment);
        return $controller->notFound();
    }
    
    /**
    * Call not access forbidden method on base controller
    * 
    * @return error page with 403 header forbidden
    */
    public function routeForbidden()
    {
        $controller = new BaseController($this->config, $this->db, $this->twig, $this->environment);
        return $controller->forbidden();
    }
    
    /**
    * Set value
    * 
    * @param mixed $key
    * @param mixed $value
    */
    public function set(string $key, $value)
    {
        if (!empty($key)) {
            $this->{$key} = $value;
        }
    }
    
    /**
    * Get value
    * 
    * @param mixed $key
    * 
    * @return mixed $this->{$key}
    */
    public function get($key)
    {
        if (isset($this->{$key})) {
            return $this->{$key};
        }
        
        return false;
    }
}
<?php

namespace BVW\Services;

class Validation
{
    
    /**
    * Validate date format
    * 
    * @param mixed $date
    * @param mixed $format
    * 
    * @return bool
    */
    public static function validateDate(string $date = '', string $format = 'Y-m-d')
    {

        if ($format == 'Y-m-d' && preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $date)) {
            return true;
        }
        
        return false;
    }
    
}

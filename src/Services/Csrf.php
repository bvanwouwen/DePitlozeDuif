<?php
  
namespace BVW\Services;

class Csrf
{  
    
    /**
    * Generate the Cross-Site Request Forgery Token
    */
    static public function setCsrfToken()
    {
        if (!isset($_SESSION['csrf_token']) || empty($_SESSION['csrf_token'])) {
            $_SESSION['csrf_token'] = bin2hex(random_bytes(32));
        }
    }
    
    /**
    * Generate CSRF Token for cross site
    * 
    * @return string token
    */
    static public function getCsrfToken()
    {
        if (!isset($_SESSION['csrf_token']) || empty($_SESSION['csrf_token'])) {
            self::setCsrfToken();
        }
        
        return $_SESSION['csrf_token'];
    }
       
}
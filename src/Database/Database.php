<?php

namespace BVW\Database;

use PDO;
use PDOExeption;

class Database
{
    
    private static $instance = null;
    
    /**
    * Disable constructor for singleton purpose
    */
    private function __construct() 
    {
    }
    
    /**
    * Disable cloning for singleton purpose
    */
    private function __clone()
    {
    } 
    
    /**
    * Connect to the DB
    * 
    * @param mixed $credentials
    * @return PDO
    */
    public static function getInstance(array $credentials): PDO
    {
        if (!self::$instance)
        {           
            $dsn = "mysql:host=".$credentials['host'].";dbname=".$credentials['dbname'].";charset=".$credentials['charset'];
            $options = [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_EMULATE_PREPARES => false,
            ];
            self::$instance = new PDO($dsn, $credentials['user'], $credentials['password'], $opt);
        }
        
        return self::$instance;
    }
}

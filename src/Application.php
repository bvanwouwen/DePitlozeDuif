<?php 

namespace BVW;

use BVW\Providers\ServiceProvider;
use BVW\Routing\Routing;
use BVW\Services\Csrf;

class Application
{
    
    /**
    * Run the application
    */
    public function run()
    {
        // Generate CSRF Token
        Csrf::setCsrfToken();
        
        // Start routing
        $routing = new Routing($this->get('config'), $this->get('db'), $this->get('twig'), $this->get('routes'));
        echo $routing->run();
    }
    
    /**
    * Set application value
    * 
    * @param mixed $key
    * @param mixed $value
    */
    public function set(string $key, $value)
    {
        if (!empty($key)) {
            $this->{$key} = $value;
        }
    }
    
    /**
    * Get application value
    * 
    * @param mixed $key
    * 
    * @return mixed $this->{$key}
    */
    public function get($key)
    {
        if (isset($this->{$key})) {
            $val = $this->{$key};
            return $val();
        }
        
        return false;
    }
    
}
<?php

namespace BVW\Auth;

use BVW\Models\User;
use PDO;

class Auth {
    
    const PASSWORD_ALGO = PASSWORD_BCRYPT;
    const PASSWORD_COST = 12;
    const MIN_PASSWORD_LENGTH = 8;
    
    protected $db;
    protected $authenticated = false;
    protected $User = false;
    
    public function __construct(PDO $db)
    {
        $this->db = $db;
        $this->User = new User($this->db);
        
        if (isset($_SESSION['User'])) {
            $this->authenticated = true;
        }
    } 
        
    /**
    * Check if user is authenticated
    * 
    * @return bool
    */
    public function isAuthenticated(): bool
    {
        return $this->authenticated;        
    }
    
    /**
     * Authenticate the user with email and password.
     *
     * @param string $password
     * @param string $email
     *
     * @return bool authenticated
     */
    public function authenticate(string $email_address, string $password)
    {
        // Already logged in?
        if ($this->isAuthenticated()) {
            return true; 
        }
                
        if (!empty($email_address)) {
            
            // Get user by email
            $user = $this->User->getByField('email_address', $email_address, true);
                        
            // Validate user and password
            if ($user && password_verify($password, $user['password'])) {
                $this->authenticated = true;
                $user['password'] = $this->checkPasswordNeedsRehash($password);
                
                // Log user in
                $this->setAuthenticated($user);
            }
            
        }
        
        return $this->authenticated;

    }
    
    /**
    * Check if the hash needs to be reset
    * 
    * @param mixed $password
    * 
    * @return string $password
    */
    protected function checkPasswordNeedsRehash($password): string
    {
        if (password_needs_rehash($password, self::PASSWORD_ALGO, ['cost' => self::PASSWORD_COST])) {
            $password = $this->setPassword($password);
        }
        
        return $password;
    }
    
    /**
     * Change the users password.
     *
     * Fails if the password doesn't pass validatePassword()
     *
     * @param string $password
     * 
     * @return string $password
     */
    protected function setPassword($password)
    {
        if (self::validatePassword($password)) {
            $password = password_hash($password, self::PASSWORD_ALGO, ['cost' => self::PASSWORD_COST]);
        }
        
        return $password;
    }
    
    /**
    * Check if passwod is valid
    * 
    * @param mixed $password
    * 
    * @return bool
    */
    public static function validatePassword($password)
    {
        return strlen($password) >= self::MIN_PASSWORD_LENGTH;
    }
    
    /**
    * Log user in
    * Update last login and password
    * Set user session
    * 
    * @param mixed $user
    */  
    protected function setAuthenticated($user)
    {
        $this->User->update($user['id'], [
            'last_login' => date('Y-m-d H:i:s'),
            'password' => $user['password']
        ]);
        
        unset($user['password']);
        $_SESSION['User'] = $user;
    }
    
}
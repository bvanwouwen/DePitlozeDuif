*API Documentatie De Pitloze Duif*
----
  Er zijn 3 lagen te beheren via de API: 
* Eigenaars
* Duiven
* Gewichtsregistratie

***

**Eigenaars**

***Create***
* **Url**
  <api-domein>/owners/store

* **Method:**
  `POST`
  
* **POST Params**
    `{name: owner_name}`

* **SUCCESS Response:**
    **Content:** `{"id":"3","name":"Nieuwe Eigenaar","created":"2018-06-24 22:39:59","modified":"2018-06-24 22:39:59"}`

* **ERROR Response:**
    **Content:** `{"errors":["Error melding 1", "Error melding"]}`
 
***List***
* **Url**
  <api-domein>/owners/index

* **Method:**
  `GET`
  
* **Params**
    De list action heeft geen parameters

* **Response:**
    **Content:** `[{"id":"1","0":"1","name":"Eigenaar 1","1":"Eigenaar 1","created":"2018-06-24 00:00:00","2":"2018-06-24 00:00:00","modified":"2018-06-24 00:00:00","3":"2018-06-24 00:00:00"},{"id":"2","0":"2","name":"Eigenaar 2","1":"Eigenaar 2","created":"2018-06-25 00:00:00","2":"2018-06-25 00:00:00","modified":"2018-06-25 00:00:00","3":"2018-06-25 00:00:00"}]`

***Update***
Deze action is nog niet geimplementeerd

* **Url**
  <api-domein>/owners/update

* **Method:**
  `POST`

***Delete***
* **Url**
  <api-domein>/owners/delete

* **Method:**
  `POST`
  
* **POST Params**
    `{id: "1"}`

***Search***
* **Url**
  <api-domein>/owners/get-list-by-name

* **Method:**
  `GET`
  
* **GET Params**
    `?search=eigenaar`

* **Response:**
    **Content:** `[{"id":"1","text":"Eigenaar 1"}, {"id":"2","text":"Eigenaar 2"}]`

***

**Duiven**

***Create***
* **Url**
  <api-domein>/pigeons/store

* **Method:**
  `POST`
  
* **POST Params**
    `{owner_id: "1", name: "Duifje", date_of_birth: "2018-05-13"}`

* **SUCCESS Response:**
    **Content:** `{"id":"1","owner_id":"1","name":"Duifje","date_of_birth":"2018-05-13","created":"2018-06-24 22:47:52","modified":"2018-06-24 22:47:52","age_in_months":1,"owner":{"id":"1","0":"1","name":"Eigenaar 1","1":"Eigenaar 1","created":"2018-06-24 00:00:00","2":"2018-06-24 00:00:00","modified":"2018-06-24 00:00:00","3":"2018-06-24 00:00:00"},"weights":[]}`

* **ERROR Response:**
    **Content:** `{"errors":["Error melding 1", "Error melding"]}`
 
***List***
* **Url**
  <api-domein>/pigeons/index
  <api-domein>/pigeons/index/{owner_id}

* **Method:**
  `GET`
  
* **Params**
    De list action heeft geen parameters

* **Response:**
    **Content:** `{"pigeons":{"1":{"id":"1","owner_id":"1","name":"Duifje","date_of_birth":"2018-05-13","created":"2018-06-24 22:47:52","modified":"2018-06-24 22:47:52","age_in_months":1,"owner":{"id":"1","0":"1","name":"Eigenaar 1","1":"Eigenaar 1","created":"2018-06-24 00:00:00","2":"2018-06-24 00:00:00","modified":"2018-06-24 00:00:00","3":"2018-06-24 00:00:00"},"weights":[]},"2":{"id":"14","0":"14","owner_id":"1","1":"1","name":"Jantje","2":"Jantje","date_of_birth":"2016-01-01","3":"2016-01-01","created":"2018-06-24 22:57:32","4":"2018-06-24 22:57:32","modified":"2018-06-24 22:57:32","5":"2018-06-24 22:57:32","age_in_months":29,"owner":{"id":"1","0":"1","name":"Eigenaar 1","1":"Eigenaar 1","created":"2018-06-24 00:00:00","2":"2018-06-24 00:00:00","modified":"2018-06-24 00:00:00","3":"2018-06-24 00:00:00"},"weights":[]}}}`

***View***
* **Url**
  <api-domein>/pigeons/view/{pigeon_id}

* **Method:**
  `GET`
  
* **Params**
    De view action heeft geen parameters

* **Response:**
    **Content:** `{"id":"14","0":"14","owner_id":"9","1":"9","name":"Jantje","2":"Jantje","date_of_birth":"2016-01-01","3":"2016-01-01","created":"2018-06-24 22:57:32","4":"2018-06-24 22:57:32","modified":"2018-06-24 22:57:32","5":"2018-06-24 22:57:32","age_in_months":29,"owner":{"id":"1","0":"1","name":"Eigenaar 1","1":"Eigenaar 1","created":"2018-06-24 00:00:00","2":"2018-06-24 00:00:00","modified":"2018-06-24 00:00:00","3":"2018-06-24 00:00:00"},"weights":[]}`

***Update***
Deze action is nog niet geimplementeerd

* **Url**
  <api-domein>/pigeons/update

* **Method:**
  `POST`

***Delete***
* **Url**
  <api-domein>/pigeons/update

* **Method:**
  `POST`

* **POST Params**
    `{id: "1"}`

***

**Gewichtsregistratie**

***Create***
* **Url**
  <api-domein>/pigeon-weights/store

* **Method:**
  `POST`
  
* **POST Params**
    `{weight: "600", weight_date: "2018-01-01"}`

* **SUCCESS Response:**
    **Content:** `{"id":"1","pigeon_id":1,"weight":"600","weight_date":"2018-01-01","created":"2018-06-24 23:09:50","modified":"2018-06-24 23:09:50"}`

* **ERROR Response:**
    **Content:** `{"errors":["Error melding 1", "Error melding"]}`
 
***List***
* **Url**
  <api-domein>/pigeon-weights/index/{pigeon_id}

* **Method:**
  `GET`
  
* **Params**
    De list action heeft geen parameters

* **Response:**
    **Content:** `[{"id":"1","0":"1","pigeon_id":"1","1":"1","weight":"600","2":"600","weight_date":"2018-01-01","3":"2018-01-01","created":"2018-06-24 23:09:50","4":"2018-06-24 23:09:50","modified":"2018-06-24 23:09:50","5":"2018-06-24 23:09:50"},{"id":"5","0":"5","pigeon_id":"1","1":"1","weight":"620","2":"620","weight_date":"2018-01-15","3":"2018-01-15","created":"2018-06-24 23:11:32","4":"2018-06-24 23:11:32","modified":"2018-06-24 23:11:32","5":"2018-06-24 23:11:32"}]`

***Update***
Deze action is nog niet geimplementeerd

* **Url**
  <api-domein>/pigeon-weights/update

* **Method:**
  `POST`

***Delete***
* **Url**
  <api-domein>/pigeon-weights/update

* **Method:**
  `POST`

* **POST Params**
    `{id: "1"}`
<?php
session_start();

require_once dirname(dirname(__FILE__))."/vendor/autoload.php";

$app = new BVW\Application;

try {
    // Inject dependencies
    $serviceProviderClass = new BVW\Providers\ServiceProvider($app);
    
    // Run appication
    $app->run();
    
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
